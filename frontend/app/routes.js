module.exports = function(app, passport){


    app.get('/', isLoggenIn, function(req, res){
        res.render('index.ejs',{
            user : req.user
        })
    })

    app.get('/index', isLoggenIn, function(req, res){
        res.render('index.ejs',{
            user : req.user
        })
    })

    app.get('/map', isLoggenIn, function(req, res){
        res.render('map.ejs',{
            user : req.user
        })
    })


    app.get('/login', function(req, res){
        res.render('login.ejs', {message: req.flash('loginMessage')})
    })

    app.post('/login', passport.authenticate('local-login', {
        successRedirect : '/index',
        failureRedirect : '/login',
        failureFlash : true
    }))

    app.get('/register', function(req, res){
        res.render('register.ejs', {message: req.flash('signupMessage')})
    })

    app.post('/register', passport.authenticate('local-signup', {
        successRedirect : '/index',
        failureRedirect : 'register',
        failureFlash : true
    }))

    app.get('/logout', function(req, res){
        req.logout()
        res.redirect('/login')
    })
}

function isLoggenIn(req, res, next){
    if(req.isAuthenticated()) 
        return next()
    
    res.redirect('/login')

}