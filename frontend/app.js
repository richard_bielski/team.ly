require('dotenv').config()
var express = require('express')
var app     = express()
var http    = require('http').Server(app)
var io      = require('socket.io')(http)
var AWS     = require('aws-sdk')
var mongoose = require('mongoose')
var passport = require('passport')
var flash = require('connect-flash')
var morgan = require('morgan')
var cookieParsar = require('cookie-parser')
var bodyParsar = require('body-parser')
var session = require('express-session')


mongoose.connect('mongodb://'+process.env.DB_USER+':'+process.env.DB_PASS+'@'+process.env.DB_HOST)

require('./config/passport')(passport)

app.use(morgan('dev'))
app.use(cookieParsar())
app.use(bodyParsar())
app.use(session({secret: 'jackzoldos'}))
app.use(passport.initialize())
app.use(passport.session())
app.use(flash())

require('./app/routes.js')(app, passport)

console.log('mongodb://'+process.env.DB_USER+':'+process.env.DB_PASS+'@'+process.env.DB_HOST)


app.set('view engine', 'ejs');
app.set('views', __dirname + '/views');

// AWS configurations
AWS.config.update({
    region: 'us-east-1'
})

var docClient = new AWS.DynamoDB.DocumentClient()

// global data JSON
var aggregatedData = {}

// serve up static files
app.use(express.static(__dirname + '/public'));


// get data from database

getAggregate(docClient)

// socket routes

io.on('connection', (socket) => {
  socket.on('requestMapData', () => {
    console.log('map data request received...')

    socket.emit('mapData', JSON.parse(aggregatedData['state']))
  })

  socket.on('requestTopData', () => {
    console.log('top data request received...')

    socket.emit('topData', JSON.parse(aggregatedData['total']))
  })

  socket.on('requestWeekData', () => {
    console.log('week data request received...')

    socket.emit('weekData', JSON.parse(aggregatedData['weekday']))
  })

  socket.on('requestGenderData', () => {
    console.log('gender data request received...')

    socket.emit('genderData', JSON.parse(aggregatedData['gender']))
  })

  socket.on('requestAgeData', () => {
    console.log('age data request received...')

    socket.emit('ageData', JSON.parse(aggregatedData['age']))
  })

  socket.on('requestHourData', () => {
    console.log('hour data request received...')

    socket.emit('hourData', JSON.parse(aggregatedData['hour']))
  })

})

// set the port
var port = process.env.PORT || 80

// make the server listen
http.listen(port, () => {
    console.log('Listening on port ' + port + '...')
})

// get aggregate data
function getAggregate(docClient) {
    var params = {
        TableName: "Team.ly-Aggrogate",
        KeyConditionExpression: "#col = :y",
        ExpressionAttributeNames: {
            "#col": "timestamp"
        },
        ExpressionAttributeValues: {
            ":y": 1
        }
    }

    docClient.query(params, function(err, data) {
        if (err) {
            console.error("Unable to query. Error:", JSON.stringify(err, null, 2));
        } else {
            aggregatedData = data.Items[0]
        }
    })
}
