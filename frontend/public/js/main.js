(function ($) {
    // USE STRICT
    "use strict";
    $(".animsition").animsition({
      inClass: 'fade-in',
      outClass: 'fade-out',
      inDuration: 900,
      outDuration: 900,
      linkElement: 'a:not([target="_blank"]):not([href^="#"]):not([class^="chosen-single"])',
      loading: true,
      loadingParentElement: 'html',
      loadingClass: 'page-loader',
      loadingInner: '<div class="page-loader__spin"></div>',
      timeout: false,
      timeoutCountdown: 5000,
      onLoadEvent: true,
      browser: ['animation-duration', '-webkit-animation-duration'],
      overlay: false,
      overlayClass: 'animsition-overlay-slide',
      overlayParentElement: 'html',
      transition: function (url) {
        window.location.href = url;
      }
    });
  
  
  })(jQuery);

// initialize the socket
var socket = io()

//Generate Data by Days of Week Bar Chart
socket.emit('requestWeekData')

socket.on('weekData', (daysData) => {
  var daysOfWeek = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"]
  var surveys = []
  var logins = []
  for(day in daysOfWeek){
    day = daysOfWeek[day]
    surveys.push(daysData[day]['surveys'])
    logins.push(daysData[day]['logins'])
  }

  var barChartConfig = {
    type: 'bar',
    defaultFontFamily: 'Poppins',
    data: {
      labels: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
      datasets: [
        {
          label: "Surveys",
          data: surveys,
          borderColor: "rgba(0, 123, 255, 0.9)",
          borderWidth: "0",
          backgroundColor: "rgba(0, 123, 255, 0.5)",
          fontFamily: "Poppins"
        },
        {
          label: "Logins",
          data: logins,
          borderColor: "rgba(0,0,0,0.09)",
          borderWidth: "0",
          backgroundColor: "rgba(0,0,0,0.07)",
          fontFamily: "Poppins"
        }
      ]
    },
    options: {
      legend: {
        position: 'top',
        labels: {
          fontFamily: 'Poppins'
        }
  
      },
      scales: {
        xAxes: [{
          ticks: {
            fontFamily: "Poppins"
  
          }
        }],
        yAxes: [{
          ticks: {
            beginAtZero: true,
            fontFamily: "Poppins"
          }
        }]
      }
    }
  };
  
  var ctx = document.getElementById("barChart").getContext("2d");
  var myChart = new Chart(ctx, barChartConfig);

})

//Generate Pie Chart of Gender Breakdown by Surveys
socket.emit('requestGenderData')

socket.on('genderData', (genderData) => {
  var surveys = [genderData['M']['surveys'], genderData['F']['surveys'], genderData['X']['surveys']]

  var ctx = document.getElementById("percent-chart-surveys");
  if (ctx) {
    ctx.height = 280;
    var pieChartConfig = {
      type: 'doughnut',
      data: {
        datasets: [
          {
            label: "Gender",
            data: surveys,
            backgroundColor: [
              '#00b5e9',
              '#fa4251',
              '#00ad5f'
            ],
            hoverBackgroundColor: [
              '#00b5e9',
              '#fa4251',
              '#00ad5f'
            ],
            borderWidth: [
              0, 0, 0
            ],
            hoverBorderColor: [
              'transparent',
              'transparent',
              'transparent'
            ]
          }
        ],
        labels: [
          'Male',
          'Female',
          'Non-Binary'
        ]
      },
      options: {
        maintainAspectRatio: false,
        responsive: false,
        cutoutPercentage: 55,
        animation: {
          animateScale: true,
          animateRotate: true
        },
        legend: {
          display: false
        },
        tooltips: {
          titleFontFamily: "Poppins",
          xPadding: 15,
          yPadding: 10,
          caretPadding: 0,
          bodyFontSize: 16
        }
      }
    }
    var myChart = new Chart(ctx, pieChartConfig);
  }

})

//Generate Pie Chart of Gender Breakdown by Logins
socket.emit('requestGenderData')

socket.on('genderData', (genderData) => {
  var logins = [genderData['M']['logins'], genderData['F']['logins'], genderData['X']['logins']]
  var ctx = document.getElementById("percent-chart-logins");

  if (ctx) {
    ctx.height = 280;
    var pieChartConfig = {
      type: 'doughnut',
      data: {
        datasets: [
          {
            label: "Gender",
            data: logins,
            backgroundColor: [
              '#00b5e9',
              '#fa4251',
              '#00ad5f'
            ],
            hoverBackgroundColor: [
              '#00b5e9',
              '#fa4251',
              '#00ad5f'
            ],
            borderWidth: [
              0, 0, 0
            ],
            hoverBorderColor: [
              'transparent',
              'transparent',
              'transparent'
            ]
          }
        ],
        labels: [
          'Male',
          'Female',
          'Non-Binary'
        ]
      },
      options: {
        maintainAspectRatio: false,
        responsive: false,
        cutoutPercentage: 55,
        animation: {
          animateScale: true,
          animateRotate: true
        },
        legend: {
          display: false
        },
        tooltips: {
          titleFontFamily: "Poppins",
          xPadding: 15,
          yPadding: 10,
          caretPadding: 0,
          bodyFontSize: 16
        }
      }
    }
    var myChart = new Chart(ctx, pieChartConfig);
    console.log(myChart)
  }
})

//Generate Age Line Chart
socket.emit('requestAgeData')

socket.on('ageData', (ageData) => {
  var data = []
  surveys = []
  logins = []
  for(age in ageData){
    var ageCorrected = age - 18
    surveys[ageCorrected] = ageData[age]['surveys']
    logins[ageCorrected] = ageData[age]['logins']
  }
  var ages = []
  for(var i=18; i<=65; i++){
    if(i%2 == 0){
      ages.push(i);
    }else{
      ages.push('');

    }
  }
  
  console.log(surveys)
  var config = {
    type: 'line',
    data: {
      labels: ages,
      type: 'line',
      defaultFontFamily: 'Poppins',
      datasets: [{
        label: "Surveys",
        data: surveys,
        backgroundColor: 'transparent',
        borderColor: 'rgba(220,53,69,0.75)',
        borderWidth: 3,
        pointStyle: 'circle',
        pointRadius: 5,
        pointBorderColor: 'transparent',
        pointBackgroundColor: 'rgba(220,53,69,0.75)',
      }, {
        label: "Logins",
        data: logins,
        backgroundColor: 'transparent',
        borderColor: 'rgba(40,167,69,0.75)',
        borderWidth: 3,
        pointStyle: 'circle',
        pointRadius: 5,
        pointBorderColor: 'transparent',
        pointBackgroundColor: 'rgba(40,167,69,0.75)',
      }]
    },
    options: {
      responsive: false,
      tooltips: {
        mode: 'index',
        titleFontSize: 12,
        titleFontColor: '#000',
        bodyFontColor: '#000',
        backgroundColor: '#fff',
        titleFontFamily: 'Poppins',
        bodyFontFamily: 'Poppins',
        cornerRadius: 3,
        intersect: false,
      },
      legend: {
        display: false,
        labels: {
          usePointStyle: true,
          fontFamily: 'Poppins',
        },
      },
      scales: {
        xAxes: [{
          display: true,
          gridLines: {
            display: false,
            drawBorder: false
          },
          scaleLabel: {
            display: false,
            labelString: 'Age'
          },
          ticks: {
            fontFamily: "Poppins"
          }
        }],
        yAxes: [{
          display: true,
          gridLines: {
            display: false,
            drawBorder: false
          },
          scaleLabel: {
            display: true,
            labelString: '',
            fontFamily: "Poppins"

          },
          ticks: {
            fontFamily: "Poppins"
          }
        }]
      },
      title: {
        display: false,
        text: 'Normal Legend'
      }
    }
  }
  ageData = ageData['Age']
  var ctx = document.getElementById("sales-chart");
  if (ctx) {
    ctx.height = 150;
    var myChart = new Chart(ctx, config);
  }
})

//Generate Hourly Usage Line Chart
socket.emit('requestHourData')

socket.on('hourData', (hourData) => {
  var data = []
  surveys = []
  for(hour in hourData){
    surveys[hour] = hourData[hour]['surveys']
  }
  var hours = []
  for(var i=0; i<=23; i++){
      hours.push(i);
  }

  var ctx = document.getElementById("hour-chart-surveys");
  var config = {
    type: 'line',
    data: {
      labels: hours,
      type: 'line',
      defaultFontFamily: 'Poppins',
      datasets: [{
        data: surveys,
        label: "Expense",
        backgroundColor: 'rgba(0,103,255,.15)',
        borderColor: 'rgba(0,103,255,0.5)',
        borderWidth: 3.5,
        pointStyle: 'circle',
        pointRadius: 5,
        pointBorderColor: 'transparent',
        pointBackgroundColor: 'rgba(0,103,255,0.5)',
      },]
    },
    options: {
      responsive: false,
      tooltips: {
        mode: 'index',
        titleFontSize: 12,
        titleFontColor: '#000',
        bodyFontColor: '#000',
        backgroundColor: '#fff',
        titleFontFamily: 'Poppins',
        bodyFontFamily: 'Poppins',
        cornerRadius: 3,
        intersect: false,
      },
      legend: {
        display: false,
        position: 'top',
        labels: {
          usePointStyle: true,
          fontFamily: 'Poppins',
        },


      },
      scales: {
        xAxes: [{
          display: true,
          gridLines: {
            display: false,
            drawBorder: false
          },
          scaleLabel: {
            display: false,
            labelString: 'Month'
          },
          ticks: {
            fontFamily: "Poppins"
          }
        }],
        yAxes: [{
          display: true,
          gridLines: {
            display: false,
            drawBorder: false
          },
          scaleLabel: {
            display: true,
            labelString: 'Value',
            fontFamily: "Poppins"
          },
          ticks: {
            fontFamily: "Poppins"
          }
        }]
      },
      title: {
        display: false,
      }
    }
  }
    if (ctx) {
      ctx.height = 150;
      var myChart = new Chart(ctx, config);
    }
})

//Generate Hourly Usage Line Chart
socket.emit('requestHourData')

socket.on('hourData', (hourData) => {
  var data = []
  logins = []
  for(hour in hourData){
    logins[hour] = hourData[hour]['logins']
  }
  var hours = []
  for(var i=0; i<=23; i++){
      hours.push(i);
  }
  var ctx = document.getElementById("hour-chart-logins");
  var config = {
    type: 'line',
    data: {
      labels: hours,
      type: 'line',
      defaultFontFamily: 'Poppins',
      datasets: [{
        data: logins,
        label: "Expense",
        backgroundColor: 'rgba(0,103,255,.15)',
        borderColor: 'rgba(0,103,255,0.5)',
        borderWidth: 3.5,
        pointStyle: 'circle',
        pointRadius: 5,
        pointBorderColor: 'transparent',
        pointBackgroundColor: 'rgba(0,103,255,0.5)',
      },]
    },
    options: {
      responsive: false,
      tooltips: {
        mode: 'index',
        titleFontSize: 12,
        titleFontColor: '#000',
        bodyFontColor: '#000',
        backgroundColor: '#fff',
        titleFontFamily: 'Poppins',
        bodyFontFamily: 'Poppins',
        cornerRadius: 3,
        intersect: false,
      },
      legend: {
        display: false,
        position: 'top',
        labels: {
          usePointStyle: true,
          fontFamily: 'Poppins',
        },


      },
      scales: {
        xAxes: [{
          display: true,
          gridLines: {
            display: false,
            drawBorder: false
          },
          scaleLabel: {
            display: false,
            labelString: 'Month'
          },
          ticks: {
            fontFamily: "Poppins"
          }
        }],
        yAxes: [{
          display: true,
          gridLines: {
            display: false,
            drawBorder: false
          },
          scaleLabel: {
            display: true,
            labelString: 'Value',
            fontFamily: "Poppins"
          },
          ticks: {
            fontFamily: "Poppins"
          }
        }]
      },
      title: {
        display: false,
      }
    }
  }
    if (ctx) {
      ctx.height = 150;
      var myChart = new Chart(ctx, config);
    }
})
