
import boto3
import json
from boto3.dynamodb.conditions import Key, Attr

dynamodb = boto3.resource('dynamodb')
table = dynamodb.Table('Team.ly-Aggrogate')

def handler(event, context):
    print("Team.ly")
    maxTime = 0
    response = table.scan()
    for i in response['Items']:
        if(i['timestamp'] > maxTime):
            maxTime = i['timestamp']
    print(maxTime)
    
    finalJSON = {}
    if len(response['Items']) > 0:
        queryJSON = response['Items'][0]
        finalJSON = {
            'state': json.loads(queryJSON['state']),
            'weekday': json.loads(queryJSON['weekday']),
            'age': json.loads(queryJSON['age']),
            'gender': json.loads(queryJSON['gender']),
            'hour': json.loads(queryJSON['hour']),
            'total': json.loads(queryJSON['total'])
        }
        print(finalJSON)
    else:
        finalJSON = {
            'state': {},
            'weekday': {},
            'age': {},
            'gender': {},
            'hour': {},
            'total': {}
        }

    
    mainTable = dynamodb.Table('Team.ly')
    fe = Key('timestamp').gt(maxTime)
    response = mainTable.scan(
        FilterExpression=fe
    )
    for i in response['Items']:
        if(i['timestamp'] > maxTime):
            maxTime = i['timestamp']

    data = response['Items']
    if(not bool(data)):
        return 0
    
    for entry in data:
        if not entry['state'] in finalJSON['state']:
            finalJSON['state'][entry['state']] = {
                'surveys': 0,
                'logins': 0,
                'users': 0
            }
        finalJSON['state'][entry['state']]['surveys'] += int(entry['surveys'])
        finalJSON['state'][entry['state']]['logins'] += int(entry['logins'])
        finalJSON['state'][entry['state']]['users'] += 1


        if not entry['weekday'] in finalJSON['weekday']:
            finalJSON['weekday'][entry['weekday']] = {
                'surveys': 0,
                'logins': 0,
                'users': 0
            }
        finalJSON['weekday'][entry['weekday']]['surveys'] += int(entry['surveys'])
        finalJSON['weekday'][entry['weekday']]['logins'] += int(entry['logins'])
        finalJSON['weekday'][entry['weekday']]['users'] += 1

        if not str(entry['age']) in finalJSON['age']:
            finalJSON['age'][str(entry['age'])] = {
                'surveys': 0,
                'logins': 0,
                'users': 0
            }
        finalJSON['age'][str(entry['age'])]['surveys'] += int(entry['surveys'])
        finalJSON['age'][str(entry['age'])]['logins'] += int(entry['logins'])
        finalJSON['age'][str(entry['age'])]['users'] += 1

        if not entry['gender'] in finalJSON['gender']:
            finalJSON['gender'][entry['gender']] = {
                'surveys': 0,
                'logins': 0,
                'users': 0
            }
        finalJSON['gender'][entry['gender']]['surveys'] += int(entry['surveys'])
        finalJSON['gender'][entry['gender']]['logins'] += int(entry['logins'])
        finalJSON['gender'][entry['gender']]['users'] += 1

        if not str(entry['hour']) in finalJSON['hour']:
            finalJSON['hour'][str(entry['hour'])] = {
                'surveys': 0,
                'logins': 0,
                'users': 0
            }
        finalJSON['hour'][str(entry['hour'])]['surveys'] += int(entry['surveys'])
        finalJSON['hour'][str(entry['hour'])]['logins'] += int(entry['logins'])
        finalJSON['hour'][str(entry['hour'])]['users'] += 1
        
        if not entry['total'] in finalJSON['total']:
            finalJSON['total'][str(entry['total'])] = {
                'surveys': 0,
                'logins': 0,
                'users': 0
            }
        finalJSON['total'][entry['total']]['surveys'] += int(entry['surveys'])
        finalJSON['total'][entry['total']]['logins'] += int(entry['logins'])
        finalJSON['total'][entry['total']]['users'] += 1
        
    print(json.dumps(finalJSON))
    response = table.put_item(Item={'state' : json.dumps(finalJSON['state']), 'weekday' : json.dumps(finalJSON['weekday']), 'age' :json.dumps(finalJSON['age']), 'gender' :json.dumps(finalJSON['gender']), 'hour' : json.dumps(finalJSON['hour']),'total' : json.dumps(finalJSON['total']), 'timestamp' : 1})
    response = table.put_item(Item={'state' : json.dumps(finalJSON['state']), 'weekday' : json.dumps(finalJSON['weekday']), 'age' :json.dumps(finalJSON['age']), 'gender' :json.dumps(finalJSON['gender']), 'hour' : json.dumps(finalJSON['hour']),'total' : json.dumps(finalJSON['total']), 'timestamp' : maxTime})
