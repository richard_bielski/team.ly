# aggregate our cooked data

data = [{ 
    "age": 28,
    "date": "08/28/18",
    "duration": 45,
    "gender": "F",
    "hour": 9,
    "income": 97337,
    "logins": 2,
    "race": "white",
    "reviewer_id": "106740",
    "state": "MA",
    "store_id": "26593",
    "surveys": 2,
    "time": "09:12:50",
    "uu_id": "106740_1532790800_389743",
    "weekday": "Tuesday"}]

finalJSON = {
    'state': {},
    'weekday': {},
    'age': {},
    'gender': {},
    'hour': {}
}

for entry in data:
    if not entry['state'] in finalJSON['state']:
        finalJSON['state'][entry['state']] = {
            'surveys': 0,
            'logins': 0,
            'users': 0
        }
    finalJSON['state'][entry['state']]['surveys'] += entry['surveys']
    finalJSON['state'][entry['state']]['logins'] += entry['logins']
    finalJSON['state'][entry['state']]['users'] += 1
    

    if not entry['weekday'] in finalJSON['weekday']:
        finalJSON['weekday'][entry['weekday']] = {
            'surveys': 0,
            'logins': 0,
            'users': 0
        }
    finalJSON['weekday'][entry['weekday']]['surveys'] += entry['surveys']
    finalJSON['weekday'][entry['weekday']]['logins'] += entry['logins']
    finalJSON['weekday'][entry['weekday']]['users'] += 1

    if not entry['age'] in finalJSON['age']:
        finalJSON['age'][entry['age']] = {
            'surveys': 0,
            'logins': 0,
            'users': 0
        }
    finalJSON['age'][entry['age']]['surveys'] += entry['surveys']
    finalJSON['age'][entry['age']]['logins'] += entry['logins']
    finalJSON['age'][entry['age']]['users'] += 1

    if not entry['gender'] in finalJSON['gender']:
        finalJSON['gender'][entry['gender']] = {
            'surveys': 0,
            'logins': 0,
            'users': 0
        }
    finalJSON['gender'][entry['gender']]['surveys'] += entry['surveys']
    finalJSON['gender'][entry['gender']]['logins'] += entry['logins']
    finalJSON['gender'][entry['gender']]['users'] += 1

    if not entry['hour'] in finalJSON['hour']:
        finalJSON['hour'][entry['hour']] = {
            'surveys': 0,
            'logins': 0,
            'users': 0
        }
    finalJSON['hour'][entry['hour']]['surveys'] += entry['surveys']
    finalJSON['hour'][entry['hour']]['logins'] += entry['logins']
    finalJSON['hour'][entry['hour']]['users'] += 1

print(finalJSON)