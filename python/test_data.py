import datetime
import json
import random
import requests
import time
from scipy.stats import norm
# from numpy.random import choice

days = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"]

races = ["white", "white", "white", "white", "white", "white", 
         "white", "white", "white", "white", "white", "white", 
         "african-american", "african-american", "african-american", 
         "hispanic/latino", "hispanic/latino", "hispanic/latino", "hispanic/latino", 
         "asian-american"]

genders = ["M", "F"]

states = ["AL","AL","AK","AZ","AZ","AZ","AR","CA","CA","CA","CA","CA","CA","CA","CA","CA","CA","CA","CA","CA",
"CO","CO","CT","CT","DE","FL","FL","FL","FL","FL","FL","FL","GA","GA","GA","GA","HI","ID","IL","IL",
"IL","IL","IN","IN","IN","IA","KS","KY","KY","LA","LA","ME","MD","MD","MA","MA","MA","MI","MI","MI",
"MI","MN","MN","MS","MO","MO","MT","NE","NV","NH","NJ","NJ","NJ","NM","NY","NY","NY","NY","NY","NY",
"NY","NC","NC","NC","NC","ND","OH","OH","OH","OH","OK","OK","OR","OR","PA","PA","PA","PA","RI","SC",
"SC","SD","TN","TN","TN","TX","TX","TX","TX","TX","TX","TX","TX","TX","UT","VT","VA","VA","VA","WA",
"WA","WA","WV","WI","WI","WY"]

def createUUID(userId):
    timestamp = int(time.time())
    randInt = random.randint(100000, 1000000)

    return str(userId) + '_' + str(timestamp) + '_' + str(randInt), timestamp

def createUsers(n):
    userIds = []

    for _ in range(0, n):
        id = random.randint(100000, 1000000)

        userIds.append(id)

    return userIds

def createStores(n):
    storeIds = []

    for _ in range(0, n):
        id = random.randint(10000, 100000)

        storeIds.append(id) 

    return storeIds

def getDate():
    month = random.randint(1, 12)
    if month in [1,3,5,7,8,10,12]:
        day = random.randint(1,31) # this range is inclusive for min and max
    elif month in [4,6,9,11]:
        day = random.randint(1,30) # this range is inclusive for min and max
    elif month==2:
        day = random.randint(1,28) # this range is inclusive for min and max
    year  = 2018

    date = datetime.datetime(year, month, day, 0, 0)

    return date

def getTime():
    hour   = random.randint(9, 21)
    minute = random.randint(0, 59)
    second = random.randint(0, 59)

    if hour < 10:
        hour = "0" + str(hour)
    if minute < 10:
        minute = "0" + str(minute)
    if second < 10:
        second = "0" + str(second)

    return str(hour) + ":" + str(minute) + ":" + str(second)


def createDataEntry(users, stores):
    entry = {}

    # get random user id
    userIndex = random.randint(0, len(users) - 1)
    entry['reviewer_id'] = str(users[userIndex])

    # create UUID
    entry['uu_id'], entry['timestamp'] = createUUID(entry['reviewer_id'])

    # get random store id
    storeIndex = random.randint(0, len(stores) - 1)
    entry['store_id'] = str(stores[storeIndex])

    # location data
    # entry['latitude'] = random.randint(35, 42)
    # entry['longitude'] = random.randint(-115, -80)
    stateIndex = random.randint(0, len(states) - 1)
    entry['state'] = states[stateIndex]

    # get date and time
    date = getDate()
    entry['date'] = date.strftime('%m/%d/%y')
    entry['weekday'] = days[date.weekday()]
    entry['time'] = getTime()
    entry['hour'] = int(entry['time'].split(':')[0])

    # demographics
    raceIndex = random.randint(0, len(races) - 1)
    entry['race'] = races[raceIndex]

    genderIndex = random.randint(0, len(genders) - 1)
    entry['gender'] = genders[genderIndex]

    entry['age'] = random.randint(18, 80)
    entry['income'] = random.randint(23000, 150000)

    # duration
    entry['duration'] = random.randint(45, 75)

    # login info
    entry['logins'] = random.randint(0, 3)

    # survery info
    entry['surveys'] = random.randint(0, 5)

    # now, adjust to create trends in data
    if entry['race'] == "white":
        entry['income'] = int(norm.rvs(size=1, loc=61349, scale=20000))
    if entry['race'] == "african-american":
        entry['income'] = int(norm.rvs(size=1, loc=38555, scale=10000))
    if entry['race'] == "hispanic/latino":
        entry['income'] = int(norm.rvs(size=1, loc=46882, scale=15000))
    if entry['race'] == "asian-american":
        entry['income'] = int(norm.rvs(size=1, loc=80720, scale=20000))

    if entry['gender'] == "F":
        entry['logins'] += random.randint(0,2)
        entry['surveys'] += random.randint(0,3)

    if entry['hour'] > 19 and entry['weekday'] != "Saturday":
        entry['surveys'] += random.randint(0,2)
        entry['logins'] += random.randint(0,3)
    if entry['hour'] > 19 and entry['weekday'] != "Friday":
        entry['surveys'] += random.randint(0,2)
        entry['logins'] += random.randint(0,3)

    if entry['hour'] >= 12 and entry['hour'] < 3 and entry['weekday'] == "Saturday":
        entry['surveys'] += random.randint(0,2)
        entry['logins'] += random.randint(0,3)
    if entry['hour'] >= 12 and entry['hour'] < 3 and entry['weekday'] == "Sunday":
        entry['surveys'] += random.randint(0,2)
        entry['logins'] += random.randint(0,3)
        
    if entry['weekday'] == "Saturday" or entry['weekday'] == "Sunday":
        entry['logins'] += random.randint(0,2)
        entry['surveys'] += random.randint(0,3)

    if entry['age'] >= 40 and entry['age'] < 50:
        entry['logins'] -= random.randint(0,2)
        entry['surveys'] -= random.randint(0,3)
        if entry['logins'] < 0:
            entry['logins'] = 0
        if entry['surveys'] < 0:
            entry['surveys'] = 0

    if entry['age'] >= 50 and entry['age'] < 60:
        entry['logins'] -= random.randint(1,3)
        entry['surveys'] -= random.randint(1,4)
        if entry['logins'] < 0:
            entry['logins'] = 0
        if entry['surveys'] < 0:
            entry['surveys'] = 0

    if entry['age'] >= 60 and entry['age'] < 70:
        entry['logins'] -= random.randint(1,4)
        entry['surveys'] -= random.randint(1,5)
        if entry['logins'] < 0:
            entry['logins'] = 0
        if entry['surveys'] < 0:
            entry['surveys'] = 0
    
    if entry['age'] >= 70:
        entry['logins'] -= random.randint(1,5)
        entry['surveys'] -= random.randint(1,6)
        if entry['logins'] < 0:
            entry['logins'] = 0
        if entry['surveys'] < 0:
            entry['surveys'] = 0

    return entry

if __name__ == "__main__":

    print('Creating users...')
    users = createUsers(100)
    # print(users)
    print('Creating stores...')
    stores = createStores(10)
    # print(stores)
    
    # entry = createDataEntry(users, stores)
    # print(entry)

    savedUUIDs = []
    savedData = []

    print('Saving data...')
    for _ in range(0, 5):
        entry = createDataEntry(users, stores)
        savedUUIDs.append(entry['uu_id'])
        savedData.append(entry)
        # print(entry)

        # set request URL
        url = "https://2b6n0m2nt7.execute-api.us-east-1.amazonaws.com/Testing/data"

        # make the POST request
        r = requests.post(url, json.dumps(entry))

        # print(r.text)

    print('Checking data...')
    for uuid in savedUUIDs:
        url = "https://2b6n0m2nt7.execute-api.us-east-1.amazonaws.com/Testing/data/" + uuid

        r = requests.get(url)
        res = json.loads(r.text)
        if (res['Count'] == 1):
            print(uuid + ": Success")
        else:
            print(uuid + ": Failure")
