import datetime
import json
import random
import requests
import time

days = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"]
races = ["white", "african-american", "hispanic/latino", "asian-american"]
genders = ["M", "F"]
states = ["Alabama", "Alaska", "Arizona", "Arkansas", "California", "Colorado", "Connecticut", "Delaware", "Florida", "Georgia", "Hawaii", "Idaho", "Illinois", "Indiana", "Iowa", "Kansas", "Kentucky", "Louisiana", "Maine", "Maryland", "Massachusetts", "Michigan", "Minnesota", "Mississippi", "Missouri", "Montana", "Nebraska", "Nevada", "New Jersey", "New Hampshire", "New Mexico", "New York", "North Carolina", "North Dakota", "Ohio", "Oklahoma", "Oregon", "Pennsylvania", "Rhode Island", "South Carolina", "South Dakota", "Tennessee", "Texas", "Utah", "Vermont", "Virginia", "Washington", "West Virginia", "Wisconsin", "Wyoming"]

def createUUID(userId):
    timestamp = int(time.time())
    randInt = random.randint(100000, 1000000)

    return str(userId) + '_' + str(timestamp) + '_' + str(randInt)

def createUsers(n):
    userIds = []

    for _ in range(0, n):
        id = random.randint(100000, 1000000)

        userIds.append(id)

    return userIds

def createStores(n):
    storeIds = []

    for _ in range(0, n):
        id = random.randint(10000, 100000)

        storeIds.append(id) 

    return storeIds

def getDate():
    month = random.randint(1, 12)
    day   = random.randint(1, 29)
    year  = 2018

    date = datetime.datetime(year, month, day, 0, 0)

    return date

def getTime():
    hour   = random.randint(9, 21)
    minute = random.randint(0, 59)
    second = random.randint(0, 59)

    if hour < 10:
        hour = "0" + str(hour)
    if minute < 10:
        minute = "0" + str(minute)
    if second < 10:
        second = "0" + str(second)

    return str(hour) + ":" + str(minute) + ":" + str(second)


def createDataEntry(users, stores):
    entry = {}

    # get random user id
    userIndex = random.randint(0, len(users) - 1)
    entry['reviewer_id'] = users[userIndex]

    # create UUID
    entry['uu_id'] = createUUID(entry['reviewer_id'])

    # get random store id
    storeIndex = random.randint(0, len(stores) - 1)
    entry['store_id'] = stores[storeIndex]

    # location data
    # entry['latitude'] = random.randint(35, 42)
    # entry['longitude'] = random.randint(-115, -80)
    stateIndex = random.randint(0, len(states) - 1)
    entry['state'] = states[stateIndex]

    # get date and time
    date = getDate()
    entry['date'] = date.strftime('%m/%d/%y')
    entry['weekday'] = days[date.weekday()]
    entry['time'] = getTime()
    entry['hour'] = int(entry['time'].split(':')[0])

    # demographics
    raceIndex = random.randint(0, len(races) - 1)
    entry['race'] = races[raceIndex]
    
    genderIndex = random.randint(0, len(genders) - 1)
    entry['gender'] = genders[genderIndex]

    entry['age'] = random.randint(18, 80)
    entry['income'] = random.randint(23000, 150000)

    # duration
    entry['duration'] = random.randint(45, 75)

    # login info
    entry['logins'] = random.randint(0, 4)

    # survery info
    entry['surveys'] = entry['logins'] - random.randint(0, 2)
    if entry['surveys'] < 0:
        entry['surveys'] = 0

    return entry

if __name__ == "__main__":

    users = createUsers(100)
    stores = createStores(10)

    for _ in range(0, 100):
        entry = createDataEntry(users, stores)
        # print(entry)

        # set request URL
        url = "https://2b6n0m2nt7.execute-api.us-east-1.amazonaws.com/Testing/data"

        # make the POST request
        r = requests.post(url, json.dumps(entry))

        # print(r.text)

    # # open and read data file
    # f = open("data.json", 'r')
    # dataString = f.read()

    # # close file
    # f.close()

    # # format string to json
    # dataJSON = json.loads(dataString)

    # # loop through the data
    # for _ in entry:
    #     # make POST request
        # r = requests.post(url, entry)
        
    #     # print result
        # print(entry)