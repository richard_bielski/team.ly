var AWS = require("aws-sdk");

AWS.config.update({
  region: "us-east-1",
});

var docClient = new AWS.DynamoDB.DocumentClient();

var params = {
    TableName : "Team.ly",
    IndexName: "date-index",
    KeyConditionExpression: "#date = :y",
    ExpressionAttributeNames: {
        "#date": "date"
    },
    ExpressionAttributeValues: {
        ":y": "06/11/18"
    }
};

docClient.query(params, function(err, data) {
    if (err) {
        console.error("Unable to query. Error:", JSON.stringify(err, null, 2));
    } else {
        data.Items.forEach(function(item) {
            console.log(item.uu_id + ": " + item.date);
	    getItem(item.uu_id)
        });
    }
});

function getItem (uu_id){
     var params = {
          TableName: "Team.ly",
	  Key: {
	       'uu_id':  uu_id	
	  }
     }

     docClient.get(params, function(err, data) {
          if (err) {
               console.error(JSON.stringify(err))
    	  } else {
               console.log(JSON.stringify(data))
    	  }
     });     		
}
